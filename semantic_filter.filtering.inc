<?php


function semantic_filter_filter_text($fgid, $text) {
  require_once(drupal_get_path('module', 'semantic_filter').'/semantic_filter.filters.inc');
  $filters = semantic_filter_filters_get($fgid);

  foreach ((array)$filters as $filter) {
    $text = semantic_filter_filter_text_with($filter, $text);
  }
  return $text;
}


function semantic_filter_filter_text_with($filter, $text) {
  $split = preg_split("/(<[^>]+>)/", $text, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
  //var_dump($split);
  //dpm($split);
  foreach ((array)$split as $key => $peace) {
    //var_dump($peace);
    if ($peace && preg_match("/^<.*>$/", $peace)) {
      $value = preg_replace("/^.*".$filter->attribute."=\"([^\"]*)\".*$/", "$1", $peace);
      //var_dump('YES', $value, "/".$filter->attribute."=\"([^\"]*)\"/");
      //var_dump($filter);
      // @TODO do regexp
      //var_dump($filter);
      //var_dump($filter->pattern, $value, strpos($value, $filter->pattern));
      if (strpos($value, $filter->value) !== FALSE) {
        if (strpos($peace, 'class') !== FALSE) {
          // add class only
          $peace = preg_replace("/(class=\"[^\"]*)(\")/", "$1 ".$filter->class."$2", $peace);
          //var_dump('CLASS', $peace);
        } else {
          // add class and tag
          $peace = preg_replace("/(>)/", " class=\"".$filter->class."\"$1", $peace);
          //var_dump('NO CLASS', $peace);
        }
        $split[$key] = $peace;
      }
    }
  }
  //dpm($split);
  return join('', $split);
}
