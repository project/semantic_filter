<?php


function semantic_filter_filter_group_filter_add_update($form_original, $fgid, $fid = NULL) {
  $form = array();
  
  $is_edit_form = isset($fid) && (int)$fid > 0;
  if ($is_edit_form) {
    $filter = semantic_filter_filter_get($fid);
  } else {
    $filter = new stdClass();
  }

  $layers = module_invoke_all('semantic_layer');
  $layer_options = array();
  foreach ((array)$layers as $key => $layer) {
    $layer_options[$key] = $layer['title'];
  }

  $attribute_types = array(
    'class' => t('Class'),
    'value' => t('Value'),
    'property' => t('Property'),
    'html' => t('HTML'),
  );

  $form['fgid'] = array(
    '#type' => 'hidden',
    '#value' => $fgid,
  );
  if ($is_edit_form) {
    $form['fid'] = array(
      '#type' => 'hidden',
      '#value' => $fid,
    );
  }
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => $is_edit_form ? t('Edit filter: @filter', array('@filter' => $filter->name)) : t('New filter'),
  );
  $form['fieldset']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => check_plain($filter->name),
  );
  $form['fieldset']['attribute'] = array(
    '#type' => 'select',
    '#title' => t('Attribute'),
    '#options' => $attribute_types,
    '#default_value' => check_plain($filter->attribute),
  );
  $form['fieldset']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('Exact match, like: management. Regular expressions: /*.org/. For matching each markup: &lt;all&gt;.'),
    '#required' => TRUE,
    '#default_value' => check_plain($filter->value),
  );
  $form['fieldset']['class'] = array(
    '#type' => 'textfield',
    '#title' => 'Class name to add',
    '#required' => TRUE,
    '#default_value' => check_plain($filter->class),
  );
  $form['fieldset']['layer'] = array(
    '#type' => 'select',
    '#title' => t('Style layer'),
    '#options' => $layer_options,
    '#default_value' => check_plain($filter->layer),
  );
  $form['fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => $is_edit_form ? t('Edit filter') : t('Add filter'),
  );

  return $form;
}


function semantic_filter_filter_group_filter_add_update_submit($form, $form_state) {
  $filter = new stdClass();
  $filter->fgid      = $form_state['values']['fgid'];
  $filter->name      = $form_state['values']['name'];
  $filter->attribute = $form_state['values']['attribute'];
  $filter->value     = $form_state['values']['value'];
  $filter->class     = $form_state['values']['class'];
  $filter->layer     = $form_state['values']['layer'];

  if (isset($form_state['values']['fid']) && (int)$form_state['values']['fid'] > 0) {
    $filter->fid = $form_state['values']['fid'];
  }

  _semantic_filter_filter_add_update($filter);

  drupal_goto('admin/settings/semantic_filter/filters/'.$form_state['values']['fgid'].'/list');
}


function semantic_filter_filter_delete($form_original, $fgid, $fid) {
  $form = array();

  $form['fgid'] = array(
    '#type' => 'value',
    '#value' => $fgid,
  );
  $form['fid'] = array(
    '#type' => 'value',
    '#value' => $fid,
  );

  return confirm_form(
    $form,
    t('Do you want to delete this filter?'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/semantic_filter/filters/'. $node->fgid .'/list',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


function semantic_filter_filter_delete_submit($form, $form_state) {
  _semantic_filter_filter_del($form_state['values']['fid']);
  drupal_set_message(t('Filter deleted.'));
}


function semantic_filter_filter_group_add_update($form_original, $fgid = NULL) {
  $form = array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
    ),
    'machine_name' => array(
      '#type' => 'textfield',
      '#title' => t('Machine readable name'),
      '#description' => t('Only lower case alphanumeric letters (a-z) and the _ sign.'),
    ),
    'description' => array(
      '#type' => 'textarea',
      '#title' => t('Description'),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Add filter group'),
    ),
  );

  if (isset($fgid) && intval($fgid) > 0) {
    $filter_group = semantic_filter_filter_group_get($fgid);

    drupal_set_title(t('Modify filter group'));
    $form['fgid'] = array(
      '#type' => 'hidden',
      '#value' => intval($fgid),
    );
    $form['submit']['#value'] = t('Save group data');
    $form['name']['#default_value'] = $filter_group->name;
    $form['machine_name']['#default_value'] = $filter_group->machine_name;
    $form['description']['#default_value'] = check_plain($filter_group->description);
  }

  return $form;
}


function semantic_filter_filter_group_add_update_validate($form, $form_state) {
  if (preg_match("/[^a-z_]/", $form_state['values']['machine_name'])) {
    form_set_error('machine_name', t('Only lower case alphanumeric letters (a-z) and the _ sign.'));
  }

  $result = db_fetch_object(db_query('SELECT COUNT(*) AS sum FROM {semantic_filter_group} WHERE machine_name = \'%s\';', $form_state['values']['machine_name']));
  if ($result->sum >= 1) {
    form_set_error('machine_name', t('Machine readable name should be unique.'));
  }
}


function semantic_filter_filter_group_add_update_submit($form, $form_state) {
  $record = new stdClass();
  $record->name = $form_state['values']['name'];
  $record->machine_name = $form_state['values']['machine_name'];
  $record->description = $form_state['values']['description'];

  if (isset($form_state['values']['fgid']) && intval($form_state['values']['fgid']) > 0) {
    $record->fgid = $form_state['values']['fgid'];
  }
  _semantic_filter_filter_group_add_update($record);

  drupal_goto('admin/settings/semantic_filter/filters/list');
}


function semantic_filter_filter_group_delete($form_original, $fgid) {
  $filter_group = semantic_filter_filter_group_get($fgid);

  $form = array();

  $form['fgid'] = array(
    '#type' => 'value',
    '#value' => $fgid,
  );

  return confirm_form(
    $form,
    t('Do you want to delete this filter group: @name?', array('@name' => check_plain($filter_group->name))),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/semantic_filter/filters/list',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


function semantic_filter_filter_group_delete_submit($form, $form_state) {
  _semantic_filter_filter_group_del($form_state['values']['fgid']);
  drupal_set_message(t('Filter group is deleted.'));
}


function semantic_filter_filter_group_list() {
  $resource = db_query('
    SELECT sfg.*, count(sf.fid) as count
    FROM {semantic_filter_group} sfg LEFT JOIN {semantic_filter} sf ON sfg.fgid = sf.fgid
    GROUP BY sfg.fgid, sfg.name, sfg.machine_name, sfg.description
    ORDER BY fgid;
  ');

  $header = array(t('Name'), t('Machine readable name'), t('Description'), t('Filters'), t('Manage filters'), t('Edit group'), t('Delete filter group'));
  $rows = array();
  while ($row = db_fetch_object($resource)) {
    $rows[] = array(
      check_plain($row->name),
      check_plain($row->machine_name),
      check_plain($row->description),
      ($row->count == 0 ? t('empty') : $row->count),
      l(t('Manage'), 'admin/settings/semantic_filter/filters/'.$row->fgid),
      l(t('Edit'), 'admin/settings/semantic_filter/filters/'.$row->fgid.'/edit', array('query' => drupal_get_destination())),
      l(t('Delete'), 'admin/settings/semantic_filter/filters/'.$row->fgid.'/delete', array('query' => drupal_get_destination())),
    );
  }

  return theme('table', $header, $rows);
}


function semantic_filter_filter_group_filters_list($fgid) {
  $resource = db_query('SELECT * FROM {semantic_filter} WHERE fgid = %d ORDER BY fid;', $fgid);

  $layers = module_invoke_all('semantic_layer');

  $header = array(t('Name'), t('Attribute'), t('Pattern'), t('Class'), t('Style layer'), '', '');

  $rows = array();
  while ($row = db_fetch_object($resource)) {
    $layer = $layers[$row->layer];
    $rows[] = array(
      check_plain($row->name),
      check_plain($row->attribute),
      check_plain($row->value),
      check_plain($row->class),
      check_plain($layer['title']),
      l(t('edit'),   'admin/settings/semantic_filter/filters/'.$fgid.'/'.$row->fid.'/edit',   array('query' => drupal_get_destination())),
      l(t('delete'), 'admin/settings/semantic_filter/filters/'.$fgid.'/'.$row->fid.'/delete', array('query' => drupal_get_destination())),
    );
  }
  return theme('table', $header, $rows);
}


function semantic_filter_filters_page($node) {
  $output = '';
  $output .= semantic_filter_filter_list($node->nid);
  $output .= drupal_get_form('semantic_filter_filter_add_form', $node->nid);

  return $output;
}
