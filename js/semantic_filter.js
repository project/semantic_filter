
Drupal.SemanticFilter = Drupal.SemanticFilter || {};

Drupal.SemanticFilter.init = function() {
  if (Drupal.settings.semantic_filters) {
    for (var id in Drupal.settings.semantic_filters) {
      //console.log(layer + ' - ' + Drupal.settings.semantic_filters[layer]);
      $('input#' + id).bind('click', function(){
        var input_id = $(this).attr('id');
        var layer_class = Drupal.settings.semantic_filters[input_id]['layer'];
        var orig_class = Drupal.settings.semantic_filters[input_id]['class'];
        //console.log(id);
        if ($(this).attr('checked')) {
          $('.' + orig_class).addClass(layer_class);
        } else {
          $('.' + orig_class).removeClass(layer_class);
        }
      });
    }
  }
}

$(function(){
  Drupal.SemanticFilter.init();
});