<?php


function semantic_filter_features_export($data, &$export, $module_name = '') {
  $layers = array();
  foreach ((array)module_implements('semantic_layer') as $module) {
    foreach ((array)module_invoke($module, 'semantic_layer') as $layer_name => $layer) {
      $layers[$layer_name] = $module;
    }
  }

  foreach ((array)$data as $filter_group_name) {
    $export['features']['semantic_filter'][$filter_group_name] = $filter_group_name;
    $filter_group = semantic_filter_filter_group_get_by_name($filter_group_name);
    $filters = semantic_filter_filters_get($filter_group->fgid);

    // Layer dependencies.
    foreach ((array)$filters as $filter) {
      if ($layers[$filter->layer]) {
        $export['dependencies'][$layers[$filter->layer]] = $layers[$filter->layer];
      }
    }
  }

  $export['dependencies']['semantic_filter'] = 'semantic_filter';

  $pipe = array();
  return $pipe;
}


function semantic_filter_features_export_options() {
  $groups = semantic_filter_filter_groups_get();
  $data = array();
  foreach ((array)$groups as $group) {
    $data[$group->machine_name] = $group->machine_name;
  }
  return $data;
}


function semantic_filter_features_export_render($module_name = '', $data = array()) {
  $code = array('  $filters = array();');

  foreach ((array)$data as $filter_group_name) {
    $filter_group = semantic_filter_filter_group_get_by_name($filter_group_name);
    $filters = semantic_filter_filters_get($filter_group->fgid, SEMANTIC_FILTER_ARRAY);

    foreach ((array)$filters as $key => $filter) {
      unset($filters[$key]['fid']);
      unset($filters[$key]['fgid']);
    }
    unset($filter_group->fgid);

    $filter_group->filters = $filters;
    $code[] = '  $filters[] = '.features_var_export((array)$filter_group, '  ').';';
  }
  $code[] = '  return $filters;';

  return array(
    'semantic_filter_defaults' => join("\n", $code),
  );
}


function semantic_filter_features_rebuild() {
  $data = module_invoke_all('semantic_filter_defaults');
  foreach ((array)$data as $filter_group_data) {
    // If not exists
    if (!semantic_filter_filter_group_get_by_name($filter_group_data['machine_name'])) {
      $filter_group               = new stdClass();
      $filter_group->name         = $filter_group_data['name'];
      $filter_group->description  = $filter_group_data['description'];
      $filter_group->machine_name = $filter_group_data['machine_name'];
      _semantic_filter_filter_group_add_update($filter_group);

      foreach ((array)$filter_group_data['filters'] as $filter_data) {
        $filter = new stdClass();
        $filter->fgid      = $filter_group->fgid;
        $filter->name      = $filter_data['name'];
        $filter->attribute = $filter_data['attribute'];
        $filter->value     = $filter_data['value'];
        $filter->class     = $filter_data['class'];
        $filter->layer     = $filter_data['layer'];
        _semantic_filter_filter_add_update($filter);
      }
    }
  }
}


function semantic_filter_features_revert() {
  $data = module_invoke_all('semantic_filter_defaults');
  foreach ((array)$data as $filter_group_data) {
    $group = semantic_filter_filter_group_get_by_name($filter_group_data['machine_name']);
    _semantic_filter_filter_group_del($group->fgid);
  }

  // Rebuild everything.
  semantic_filter_features_rebuild();
}
